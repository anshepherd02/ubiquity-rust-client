# PlatformsOverview

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platforms** | Option<[**Vec<crate::models::PlatformsOverviewPlatforms>**](platforms_overview_platforms.md)> | List of items each describing a pair of supported platform and network. | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


