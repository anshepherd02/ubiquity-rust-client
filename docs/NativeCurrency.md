# NativeCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_path** | **String** | Asset path of transferred currency | 
**symbol** | Option<**String**> | Currency symbol | [optional]
**name** | Option<**String**> | Name of currency | [optional]
**decimals** | Option<**i32**> | Decimal places right to the comma | [optional]
**_type** | **String** |  | [default to native]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


